# mongo_export

The mongo_export.sh allows you to export all the collections of specific a app.
The template.sh script generate files config entrypoint.sh and variables.env

## Table of Content

- [mongo\_export](#mongo_export)
  - [Table of Content](#table-of-content)
  - [Technologies](#technologies)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
    - [install dependencies](#install-dependencies)
    - [Setup containers using docker-compose](#setup-containers-using-docker-compose)
  - [Running](#running)
  - [Test](#test)
    - [Prerequisites-Test](#prerequisites-test)
    - [Testing](#testing)
  - [Cleanup](#cleanup)
    - [Delete containers using docker-compose](#delete-containers-using-docker-compose)
    - [Delete dependencies](#delete-dependencies)
    - [Delete bats dependencies](#delete-bats-dependencies)
  - [Author](#author)

## Technologies

- bash 5.0
- awk 5.0.1
- bats

## Prerequisites

Required installed software:

- make

## Setup

### install dependencies

```sh
 make install-dependencies
```

### Setup containers using docker-compose

```sh
    make start
```

## Running

```sh
 bash  mongo_export_collections.sh
```

or

```sh
 ./mongo_export_collections.sh
```

## Test

### Prerequisites-Test

```sh
    make install-dependencies-tests
```

### Testing

```sh
    make tests
```

## Cleanup

### Delete containers using docker-compose

```sh
    make clean
```

### Delete dependencies

```sh
    make remove-dependencies
```

### Delete bats dependencies

```sh
    make remove-dependencies-tests
```

## Author

*****
__goodbit22__ --> "<https://gitlab.com/users/goodbit22>"
*****
