#!/bin/bash
scripts_main="functions.sh"
for script in "${scripts_main[@]}"
do
	# shellcheck source=/dev/null
	if [ -f "${script}" ]; then
		source "${script}"
	else
		echo -e "Nie ma wymaganego pliku ${script}"
		exit 1;
	fi
done

check_install_packets_mongo_tools