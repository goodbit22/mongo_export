#!/bin/bash

YELLOW=$(tput setaf 3)
CYAN=$(tput setaf 6)
RED=$(tput setaf 1) 
GREEN=$(tput setaf 2) 
WHITE=$(tput setaf 7)
status=("${GREEN}[OK]${WHITE}" "${YELLOW}[WARNING]${WHITE}" "${RED}ERROR${WHITE}" )

usage() {
    echo "Usage: $0 [-a] [-f] [-r] [-c] [-h]"
    echo "Options:"
    echo "  -a           App name"
    echo "  -r           Mode run generated query"
    echo "  -f           Save generated query to file"
    echo "  -h           Display the help message"
    echo "  -c           Database login configuration file"
    echo "  -d           Data from"
    echo "  -l           limits result query"
    exit 0
}

check_install_packets_mongo_tools(){
    sudo apt install -y zip 
    MONGOSH_VERSION=$(curl -s "https://api.github.com/repos/mongodb-js/mongosh/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
    MONGOTOOL_VERSION=$(curl https://api.github.com/repos/mongodb/mongo-tools/git/refs/tags | jq -r '.[-1].ref' | cut -d '/' -f 3 )
    SYSTEM_MAJOR_VERSION=$(cat  < /etc/debian_version | cut -d '.' -f 1)
    readonly name_directory="deb_packages"
    local packages_deb=("https://downloads.mongodb.com/compass/mongodb-mongosh_${MONGOSH_VERSION}_amd64.deb"
    "https://fastdl.mongodb.org/tools/db/mongodb-database-tools-debian${SYSTEM_MAJOR_VERSION}-x86_64-${MONGOTOOL_VERSION}.deb")    
    
    [ ! -d  "$name_directory" ] && mkdir "$name_directory" &&  echo -e "${GREEN}Katalog $name_directory zostal utworzony${WHITE}"
    for package_deb in "${packages_deb[@]}" 
    do
        curl -L "$package_deb" -o "${name_directory}/${package_deb##*/}" && sudo dpkg -i "${name_directory}/${package_deb##*/}"
    done
    [ -d  "$name_directory" ] && rm -rf "$name_directory" && \
    echo -e "${GREEN}Katalog $name_directory zostal usuniety wraz z zawartoscia${WHITE}"
}

mongo_check_connection(){
    uri="$(grep  'uri' "$config" | cut -d ':' -f2- | tr -d '[:space:]')" 
    mongosh "${uri}" --eval "db.serverStatus" --quiet  &> /dev/null || (echo "mongo not connected"; exit 1) 
    echo -e "${CYAN}Mongo connected ${WHITE} ${status["$?"]}"
}


mongo_export_name_collections(){
    app="$1"
    collections_file="$2"
    query_file="${app}_query.js"
    pattern="e$(echo "${app}" | cut -c 1)"
    uri="$(grep  'uri' "$config" | cut -d ':' -f2- | tr -d '[:space:]')" 

cat <<EOL > "$query_file"
// Get the list of collections in the database
var collections = db.getCollectionNames();

// Filter collections based on the pattern and print them
collections.forEach(function(collection) {
    if (collection.match(/^$pattern/)) {
        print(collection);
    }
});
EOL
    mongosh  "$uri" --file "$query_file" --quiet > "$collections_file"
    if [ $? -eq 0 ]; then
        echo "Collections starting with $pattern have been written to $collections_file"
    else
        echo "Failed to retrieve collections from MongoDB."
    fi
    rm -f "$query_file"
}



check_network(){
	ping -c 2 -q  www.bnpparibas.pl  >> /dev/null
	echo -e "${CYAN}Dostep do sieci${WHITE} ${status["$?"]}"
}

create_zip() {
    directory="$1"; run_mode="$2"; file_query="$3"
    password=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
    if [[ "$run_mode" = "true" ]]; then
        zip  "${directory}.zip" "$directory" -P "$password"
        echo "ZIP PASSWORD: $password"
    else 
cat <<-EOF >> "$file_query"
zip -r "${directory}.zip" "$directory" -P "$password"
echo "ZIP PASSWORD: $password"
EOF
    fi
}     

generate_queries_export(){
    config="$1"; collection="$2"; app_collection="$3"; limits="$4"; data_from="$5";
    query_export="mongoexport  --config=${config} --collection=$collection --out=${app_collection}.json --sort='{mongocreated: -1}'" 
    if [[ "$limits" != "" ]] ; then
        query_export="${query_export} --limit=${limits}"
    fi
    if [[ "$data_from" != "" ]]; then
         date_func="new Date( "$data_from") } }"
         data_query='{ "date": { $lt: '
         query_export="${query_export} --query '${data_query} ${date_func}'"

    fi
    echo "$query_export"
}

run_query_export(){
    run_mode="$1"
    if [[ "$run_mode" = "true" ]]; then
        local query_export="$2"
        local collection="$3"
        echo "Rozpoczeto eksportowanie kolekcji $collection"
        bash -c "$query_export"
        echo "Zakoczenie eksportowanie kolekcji $collection"
    else 
        local query_export="$2"
        local file_query="$3"
        echo "$query_export" >> "$file_query"
    fi
}
