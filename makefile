variables_file:=variables.env
dockercompose_file:=docker-compose.yml
secret_login_file:=$(shell grep  "SECRET_FILE_L" $(variables_file) | cut -d ':' -f2 | tr -d "[:space:]")
secret_password_file:=$(shell grep  "SECRET_FILE_P" $(variables_file) | cut -d ':' -f2 | tr -d "[:space:]")
port_mongo_express:=$(shell grep  "MONGODB_EXPRESS_PORT" $(variables_file) | cut -d ':' -f2 | tr -d "[:space:]")
port_mongo:=$(shell grep  "MONGODB_PORT" $(variables_file) | cut -d ':' -f2 | tr -d "[:space:]")
port_mongo_bind:=$(shell ss -t -l -p -n | grep  ":$(port_mongo)" | cut -d ':' -f2 | cut -d ' ' -f1 )
directory_bats-dependencies-tests=bats-dependencies-tests
FILE1_EXISTS := $(wildcard $(variables_file))
FILE2_EXISTS := $(wildcard $(dockercompose_file))
dependencies_path=dependencies
dependencies_script=scripts/dependencies_install.sh
template_script=template.sh


template:
	$(shell chmod u+x ./${template_script})
	$(shell sudo ./${template_script})


start:
# $(MAKE) template
ifdef port_mongo_bind
	@echo "Port jest juz zajety $(port_mongo_bind)" 
endif
ifeq (,$(FILE1_EXISTS)$(FILE2_EXISTS))
	@echo "Both files do not exist."
else ifeq ($(FILE1_EXISTS),)
	@echo "Plik $(variables_file) nie istnieje"
else ifeq ($(FILE2_EXISTS),)
	@echo "Plik $(dockercompose_file) nie istnieje"
else
	$(shell echo -n 'admin' > $(secret_login_file) )  
ifeq (,$(wildcard $(secret_password_file)))
	$(shell date +%s | sha256sum | base64 | head -c 32 >  $(secret_password_file))
endif	
	#docker-compose --env-file $(variables_file)  -f $(dockercompose_file)  pull   
	docker-compose --env-file $(variables_file)  -f $(dockercompose_file) up -d
endif

tests:
	@bats test

install-dependencies:
	$(shell chmod u+x ./${dependencies_directory}/${dependencies_script})
	$(shell sudo ./${dependencies_directory}/${dependencies_script})

# remove-dependencies:
#     sudo apt remove -y mongodb-mongosh mongodb-database-tools zip


install-dependencies-tests:
ifeq (,$(wildcard $(directory_bats-dependencies-tests)))
	@echo "$(directory_bats-dependencies-tests) directory create"
	mkdir $(directory_bats-dependencies-tests)
endif
	git clone https://github.com/bats-core/bats-file $(directory_bats-dependencies-tests)/bats-file
	git clone https://github.com/bats-core/bats-support.git $(directory_bats-dependencies-tests)/bats-support
	git clone https://github.com/bats-core/bats-assert.git  $(directory_bats-dependencies-tests)/bats-assert

remove-dependencies-tests:
	rm -r $(directory_bats-dependencies-tests)

status:
	docker-compose --env-file $(variables_file)  -f $(dockercompose_file)  ps 

logs:
	docker-compose --env-file $(variables_file)  -f $(dockercompose_file)  logs

clean:
	docker-compose rm  --volumes --stop

