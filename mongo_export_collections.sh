#!/bin/bash 
set -e

scripts_main="dependencies/scripts/functions.sh"
for script in "${scripts_main[@]}"
do
	# shellcheck source=/dev/null
	if [[ -f "${script}" ]]; then
		source "${script}"
	else
		echo -e "Nie ma wymaganego pliku ${script}"
		exit 1;
	fi
done

WHITE=$(tput setaf 7)
COLUMNS=$(tput cols)
run_mode="false"
file_query="${VARIABLE:=query.sh}"
current_data=$(date +%y-%m-%d-%H:%M:%S)
apps=("pay" "onb" "ac" "recon")
declare -A ec_apps



while getopts ':a:c:l:f:rh:d:-' OPTION; do
    case "$OPTION" in 
    a|app)
        echo "Application: $OPTARG"
        app="$OPTARG"
        ;;
    f|file) 
        echo "file Save generated query:  $OPTARG"
        file_query="$OPTARG"
        ;;
    r|run)
        run_mode="true"
        ;;
    c|config)
        echo "Mongodb login file: $OPTARG"
        config="$OPTARG"
        ;;
    l|limits)
        declare -i limits
        limits="$OPTARG"
        echo "Limits: $limits"
        ;;
    d|data)
        echo "Data from: $OPTARG"
        data_from="$OPTARG"
        ;;
    h|help)
        usage
        ;;
    -)
      case "${OPTARG}" in
        app)
          app="$OPTARG"
          ;;
        file)
          file_query="$OPTARG"
          ;;
        run)
          run_mode="true"
          ;;
        config)
          config="$OPTARG"
          ;;
        limits)
          declare -i limits
          limits="$OPTARG"
          ;;
        data)
          echo "$OPTARG"
          data_from="$OPTARG"
          ;;
        help)
          usage
          ;;
        *)
          echo "Invalid option: --$OPTARG"
          exit 1
          ;;
      esac
      ;;
    \?)
        echo "Usage: $(basename $0) [-a app] [-c mongo config file] "
        exit 1
        ;;
    :)
      echo "Invalid option: $OPTARG requires an argument"
      ;;
    esac 
done


if [[ -z "$app" ]]; then
    echo "nie zdefiniowales aplikacji"
    exit 1
fi
if [[  -z "$config" ]] && [[ ! -f "$config" ]]; then
    echo "nie zdefiniowales plik konfiguracyjnego"
    exit 1
fi
if [[ ! -f "$file_query" ]]; then
  echo  "#!/bin/bash" > "$file_query"
else 
  echo  "#!/bin/bash" > "$file_query"
fi

main(){
    mongo_check_connection
    check_network
}

main

if [[ ${apps[@]} =~ $app ]]; then 
  output_file="${app}_collections"
  if [[ ! -f "$output_file" ]]; then
    mongo_export_name_collections "$app" "$output_file"
    app_collections=""
    while IFS= read -r line; do 
        collections+="${line};"
    done < "$output_file"
      ec_apps["$app"]="$app_collections"
  else
    echo "The $output_file exists to generate a file with new collections, delete file $output_file"
  fi
else 
    echo "The collection does not exist for $app application"
    exit 1
fi


directory_name="${app}_${current_data}"
mkdir "$directory_name"

IFS=";" read -r -a collections <<< "${ec_apps[$app]}"
for collection in "${collections[@]}"; do 
    echo "collection: $collection"
    if [[ $limits != "" ]] && [[ $data_from = "" ]]; then
      generate_query_export=$(generate_queries_export "$config" "$collection" "${directory_name}/${app}_${collection}" "$limits")
    elif [[ $limits != "" ]] && [[ $data_from != "" ]]; then
       generate_query_export=$(generate_queries_export "$config" "$collection" "${directory_name}/${app}_${collection}" "$limits" "$data_from")
    else 
      generate_query_export=$(generate_queries_export "$config" "$collection" "${directory_name}/${app}_${collection}")
    fi
    echo "${generate_query_export}"
    if [[ "$run_mode" == "true" ]]; then
      run_query_export "$run_mode" "${generate_query_export}" "${collection}"
    else 
      run_query_export "$run_mode" "${generate_query_export}" "${file_query}"
    fi
done


create_zip "$directory_name" "$run_mode" "$file_query"




