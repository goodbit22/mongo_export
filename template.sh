#!/bin/bash
directory_secrets="dependencies/secrets"
directory_templates="dependencies/templates"
mongo_entrypoint_path="dependencies/mongo-entrypoint"
[ ! -f "${directory_secrets}" ] && mkdir -p "${directory_secrets}"

export ME_CONFIG_BASICAUTH=false
export SECRET_FILE_L=$(readlink  -f $directory_secrets/db_username.txt);  echo -n 'admin' > "$SECRET_FILE_L"
export SECRET_FILE_P=$(readlink  -f $directory_secrets/db_password.txt);  date +%s | sha256sum | base64 | head -c 32 >  "$SECRET_FILE_P"  
export MONGODB_EXPRESS_PORT=8081; export MONGODB_EXPRESS_TAGS='latest'
export MONGODB_PORT=27017; export MONGODB_TAGS='latest';
export MONGO_ROLE='readWrite'
export MONGO_USER='devUser'
export MONGO_PASSWORD='devUserPass'
export MONGO_DATABASE='app_db_name'
export MONGO_INITDB_ROOT_USERNAME="$(< $SECRET_FILE_L)"
export MONGO_INITDB_ROOT_PASSWORD="$(< $SECRET_FILE_P)"
templates=('config-template' 'entrypoint-template.sh' 'variables-template.env')
    for template in "${templates[@]}"; 
    do  
        if [[ $template = 'entrypoint-template.sh' ]]; then
            envsubst <  "$directory_templates/${template}" > "$mongo_entrypoint_path/${template/-template/}"
            echo -e "Template: $template -> $mongo_entrypoint_path${template/-template/}"
        elif [[ $template = 'variables-template.sh' ]]; then
            envsubst <  "$directory_templates/${template}" > "${template/-template/}"
            echo -e "Template: $template -> ${template/-template/}"
        elif [[ $template = 'config' ]]; then
            envsubst <  "$directory_templates/${template}" > "${template/-template/}"
            envsubst <  "$directory_templates/${template}" > "test/${template/-template/}"
            echo -e "Template: $template -> ${template/-template/}"
        fi
        envsubst <  "$directory_templates/${template}" > "$directory_templates/${template/-template/}"
    done 

