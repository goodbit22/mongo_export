#!/usr/bin/env bats
bats_require_minimum_version 1.11.0
bash_script="functions.sh"
config="config"
collection="payment-test"
app_collection="payment-test2"
file="test_query"
query_export_test="mongoexport  --config=${config} --collection=$collection --out=${app_collection}.json --sort='{mongocreated: -1}'" 
dependencies_directory=$(grep -m 1  "directory_bats-dependencies-tests" makefile | cut -d '=' -f2 | tr -d "[:space:]" )
dependencies_script="dependencies/scripts"
bats_load_library  $(realpath $dependencies_directory)/bats-file/load.bash
bats_load_library  $(realpath $dependencies_directory)/bats-assert/load.bash
bats_load_library  $(realpath $dependencies_directory)/bats-support/load.bash


function setup() {
    source "${dependencies_script}/$bash_script" 
}

@test "Function create zip" {
    run_mode="true"; directory="zip_test"
    mkdir -p "$directory"
    assert_dir_exists "$directory"
    run create_zip "$directory" "true" 
    assert_success 
    assert_file_exists "${directory}.zip"
    rmdir "$directory" ; rm -r "${directory}.zip"
}

@test "Function usage() return 0" {
    run usage
    assert_success 
}

@test "Function generate_queries_export" {
    run generate_queries_export "$config" "$collection" "$app_collection" 
    assert_output "$query_export_test"
}

@test "funtion generate_queries_export only limits" {
    limits=100
    query_export_test="${query_export_test} --limit=${limits}"
    run generate_queries_export "$config" "$collection" "$app_collection" "$limits"
    assert_output "$query_export_test"
}

@test "funtion generate_queries_export data and limits" {
    limits=100; data_from="2016-01-01T00:00:00.000Z"
    local query_export_test="mongoexport  --config=${config} --collection=$collection --out=${app_collection}.json --sort='{mongocreated: -1}' --limit=${limits} --query '{ "'"date"'": { "'$lt'":  new Date( "2016-01-01T00:00:00.000Z") } }'" 
    run generate_queries_export "$config" "$collection" "$app_collection" "$limits" "$data_from"
    assert_output "$query_export_test"
}

@test "function run_query_export is false" {
    run_mode="false";
    run run_query_export "$run_mode" "$query_export_test" "$file"
    assert_file_exists "$file"
    rm -r $file
}

@test "function run_query_export is true" {
    run_mode="true";
    run run_query_export  "$run_mode" "$query_export_test" "$collection"
    assert_success 
}

