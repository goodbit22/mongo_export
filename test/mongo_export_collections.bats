#!/usr/bin/env bats
bats_require_minimum_version 1.11.0
bash_script="mongo_export_collections.sh"
dependencies_directory=$(grep -m 1  "directory_bats-dependencies-tests" makefile | cut -d '=' -f2 | tr -d "[:space:]" )

bats_load_library  $(realpath $dependencies_directory)/bats-file/load.bash
bats_load_library  $(realpath $dependencies_directory)/bats-assert/load.bash
bats_load_library  $(realpath $dependencies_directory)/bats-support/load.bash


@test "Can run script" {
    run  bash "$bash_script"
}

@test "Invalid parameter script" {
    run "./$bash_script"  --invalid 
    assert_success 1
}

@test "File docker-compose exits" {
    assert_exists "./docker-compose.yml"
}

@test "File docker-compose is not empty" {
    assert_file_not_empty "./docker-compose.yml"
}

@test "File config exists" {
    assert_exists "./config"
}


